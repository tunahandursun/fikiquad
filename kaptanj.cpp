#include "vecihi.h"

void VecihiHurkus::dengemerkezi(){
  cout<<"Denge merkezi devrede\n";

  ms_open();
  sleep(1);

  float gyrohassasiyet = 0.7;
  float hareketcarpani = 8;
  float arttirmacarpani = 0.2;
  float azaltmacarpani = 1;
  // ypr[PITCH]>2 || ypr[PITCH]<-2 || ypr[ROLL]>2 || ypr[ROLL]<-2
  while(1){
    ms_update();
//    cout<<"yaw: "<< ypr[YAW]<< "\tpitch ="<< ypr[PITCH] <<"\troll:" << ypr[ROLL] << endl;
    // sqrtf(pow(ypr[ROLL],2)+pow(ypr[PITCH],2))
    if(aktifhareket == 0 || aktifhareket == 1){
        if(ypr[PITCH] + gyrohassasiyet < zerovaluepitch){
          // I nın değeri azaltılacak, motorsinyalleri[0]
          // cout << "I numara" << endl;
           cout << "\033[1;31mI numara\033[0m" << ypr[PITCH]-zerovaluepitch <<'\n';
          if(motorsinyalleri[0] <= minsinyal){
            cout<<"I. motor min sinyalde" << endl;
          }
          else{
            motorsinyalleri[0] = motorsinyalleri[0] - (harekethassasiyeti*hareketcarpani)*azaltmacarpani;
            motorsinyalleri[2] = motorsinyalleri[2] + (harekethassasiyeti*hareketcarpani)*arttirmacarpani;
//           set();
        		flush(fid);
        		fid<<motorid[0]<<"="<<motorsinyalleri[0]<<"\%\n";
        		flush(fid);
            fid<<motorid[2]<<"="<<motorsinyalleri[2]<<"\%\n";
            flush(fid);

          }
        }
        if(ypr[ROLL] + gyrohassasiyet < zerovalueroll){
          // II nın değeri azaltılacak, motorsinyalleri[1]
          // cout << "II numara" <<  ypr[ROLL] -zerovalueroll <<'\n';
           cout << "\033[1;31mII numara\033[0m"<<  ypr[ROLL]-zerovalueroll <<'\n';
          if(motorsinyalleri[1] <= minsinyal){
            cout<<"II. motor min sinyalde" << endl;
          }
          else{
            motorsinyalleri[1] = motorsinyalleri[1] - (harekethassasiyeti*hareketcarpani)*azaltmacarpani;
            motorsinyalleri[3] = motorsinyalleri[3] + (harekethassasiyeti*hareketcarpani)*arttirmacarpani;
         //   set();
        		flush(fid);
        		fid<<motorid[1]<<"="<<motorsinyalleri[1]<<"\%\n";
        		flush(fid);
            fid<<motorid[3]<<"="<<motorsinyalleri[3]<<"\%\n";
        		flush(fid);
          }
        }
        if(ypr[PITCH] - gyrohassasiyet > zerovaluepitch){
          // III nın değeri azaltılacak, motorsinyalleri[2]
//           cout << "III numara " << ypr[PITCH]-zerovaluepitch <<'\n';
           cout << "\033[1;31mIII numara\033[0m" <<  ypr[PITCH]-zerovaluepitch <<'\n';
          if(motorsinyalleri[2] <= minsinyal){
            cout<<"III. motor min sinyalde" << endl;
          }
          else{
            motorsinyalleri[2] = motorsinyalleri[2] - (harekethassasiyeti*hareketcarpani)*azaltmacarpani;
            motorsinyalleri[0] = motorsinyalleri[0] + (harekethassasiyeti*hareketcarpani)*arttirmacarpani;
        //    set();
        		flush(fid);
        		fid<<motorid[2]<<"="<<motorsinyalleri[2]<<"\%\n";
        		flush(fid);
            fid<<motorid[0]<<"="<<motorsinyalleri[0]<<"\%\n";
            flush(fid);

          }
        }
        if(ypr[ROLL] - gyrohassasiyet > zerovalueroll){
          // IV nın değeri azaltılacak, motorsinyalleri[3]
           cout << "\033[1;31mIV numara\033[0m" <<  ypr[ROLL]-zerovalueroll <<'\n';
          if(motorsinyalleri[3] <= minsinyal){
            cout<<"IV. motor min sinyalde" << endl;
          }
          else{
            motorsinyalleri[3] = motorsinyalleri[3] - (harekethassasiyeti*hareketcarpani)*azaltmacarpani;
            motorsinyalleri[1] = motorsinyalleri[1] + (harekethassasiyeti*hareketcarpani)*arttirmacarpani;
//            set();
        		flush(fid);
        		fid<<motorid[3]<<"="<<motorsinyalleri[3]<<"\%\n";
        		flush(fid);
            fid<<motorid[1]<<"="<<motorsinyalleri[1]<<"\%\n";
        		flush(fid);
          }
        }
    }

    delay_ms(5);
  }
}
