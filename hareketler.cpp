#include "vecihi.h"
void VecihiHurkus::hareketizni(int hareketno){
	switch (hareketno) {
		case 0: {
		//ileri
			aktifhareket = 2;

			if(motorsinyalleri[1]+harekethassasiyeti<maxsinyal ){
				if (motorsinyalleri[2]+harekethassasiyeti<maxsinyal) {
					motorsinyalleri[1]+=harekethassasiyeti;
					motorsinyalleri[2]+=harekethassasiyeti;
				}else{
					cout<<"ileri işlemine izin verilmedi!\n";
					return;
					}
				}else{
				cout<<"ileri işlemine izin verilmedi!\n";
				return;
			}
			set();
			return;
			break;
		}

		case 1:{
		//geri
			aktifhareket = 2;

			if(motorsinyalleri[0]+harekethassasiyeti<maxsinyal ){
				if (motorsinyalleri[3]+harekethassasiyeti<maxsinyal) {
					motorsinyalleri[0]+=harekethassasiyeti;
					motorsinyalleri[3]+=harekethassasiyeti;
				}else{
					cout<<"geri işlemine izin verilmedi!\n";
					return;
				}
			}else{
				cout<<"geri işlemine izin verilmedi!\n";
				return;
			}
			set();
			return;
			break;
		}
		case 2:{
		// sağ
			aktifhareket = 2;

			if(motorsinyalleri[3]+harekethassasiyeti<maxsinyal ){
				if (motorsinyalleri[2]+harekethassasiyeti<maxsinyal) {
					motorsinyalleri[3]+=harekethassasiyeti;
					motorsinyalleri[2]+=harekethassasiyeti;
				}else{
					cout<<"Sağ işlemine izin verilmedi!\n";
					return;
					}
				}else{
				cout<<"Sağ işlemine izin verilmedi!\n";
				return;
			}
			set();
			return;
			break;
		}
		case 3:{
		// sol
			aktifhareket = 2;

			if(motorsinyalleri[1]+harekethassasiyeti<maxsinyal ){
				if (motorsinyalleri[0]+harekethassasiyeti<maxsinyal) {
					motorsinyalleri[1]+=harekethassasiyeti;
					motorsinyalleri[0]+=harekethassasiyeti;
				}else{
					cout<<"Sol işlemine izin verilmedi!\n";
					return;
					}
				}else{
				cout<<"Sol işlemine izin verilmedi!\n";
				return;
			}
			set();
			return;
			break;
		}
		default:
			cout<< "Tanımsız hareket #hareketizni: "<< hareketno << endl;
			return;
	}
}
void VecihiHurkus::guc(int hareketno){
	switch (hareketno) {
		case 0:{
			// güç arttırılıyor
			aktifhareket = 0;

			if(motorsinyalleri[0]+harekethassasiyeti < maxsinyal){
				int i=0;
				for(i=0;i<motorsayisi;i++)
					motorsinyalleri[i]+= harekethassasiyeti;
				set();
				return;
			}
			else{
				cout<<"motor sinyalleri max seviyeye ulaşı!";
				return;
			}
			break;
		}
		case 1:{
			//güç azaltılıyor
			aktifhareket = 1;

			if(motorsinyalleri[0]-harekethassasiyeti > minsinyal){
				int i=0;
				for(i=0;i<motorsayisi;i++)
					motorsinyalleri[i]-=harekethassasiyeti;
				set();
				return;
			}
			else{
				cout<<"motor sinyalleri min seviyede!";
				return;
			}

			break;
		}
		case 2:{
			zerovalueroll = ypr[ROLL];
			zerovaluepitch = ypr[PITCH];
			cout<<"zerovaluepitch: "<< zerovaluepitch << "zerovalueroll: " << zerovalueroll << "\n";
			return;
			break;
		}
		default:
			cout<<"Tanımsız hareket #güç: "<<hareketno<<endl;
			break;
	}
}
void VecihiHurkus::dengele(){}

void VecihiHurkus::set(){
	int i;
	for(i=0;i<motorsayisi;i++){
		flush(fid);
		fid<<motorid[i]<<"="<<motorsinyalleri[i]<<"\%\n";
		flush(fid);
		cout<<motorid[i]<<"="<<motorsinyalleri[i]<<"\%"<<endl;
	}
	cout<<"başarılı\n";
}

void VecihiHurkus::kapat(){
	int i;
	for(i=0;i<motorsayisi;i++)
		motorsinyalleri[i]=minsinyal;
	set();
	fid.close();
	return;
}

void VecihiHurkus::durdur(){
	// Motorları min seviyeye çeker
	aktifhareket = -1;
	int i=0;
	for(i=0;i<motorsayisi;i++)
		motorsinyalleri[i] = minsinyal;
	set();
	cout << "Minimum seviyede!" << '\n';
}
