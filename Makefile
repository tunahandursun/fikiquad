CXX=g++
CXXFLAGS= -std=c++0x -Wall -g -O2
CXX_OPTS= -Wall -g -O2
HEYYO= papagan.cpp hareketler.cpp kaptanj.cpp
INSTALL=install
LDFLAGS	= -pthread
PROG=fikiquad

%.o: %.c                                                                         
	$(CXX) $(LDFLAGS) $(CXXFLAGS) $(CXX_OPTS) $< -o $@ 


all: $(PROG).o 
	$(CXX) $(LDFLAGS) $(CXXFLAGS) -o $(PROG) \
		main.cpp $(HEYYO) \
		MotionSensor/libMotionSensor.a \
		libs/libI2Cdev.a

$(PROG).o: MotionSensor/libMotionSensor.a libs/libI2Cdev.a

MotionSensor/libMotionSensor.a:
	$(MAKE) -C MotionSensor/ 

libs/libI2Cdev.a:
	$(MAKE) -C libs/I2Cdev

install1:
	$(INSTALL) -m 755 $(PROG) $(DESTDIR)/usr/local/bin/

clean:
	cd MotionSensor && $(MAKE) clean
	cd libs/I2Cdev && $(MAKE) clean
	rm -rf *.o *~ *.mod
	rm -rf $(PROG)
