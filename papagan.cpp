#include "vecihi.h"

void VecihiHurkus::iletisim(){
	cout<<"İletişim sağlandı"<<endl;
	while(1){
		bzero(buffer,1);
		int n = read(newsockfd,buffer,1);
		if (n < 0){
			printf("okunurken hata!\n");
			close(newsockfd);
			close(sockfd);
			exit(1);
		}
		cout<<"gelen: "<<buffer[0]<<endl;

		if(buffer[0]=='0') hareketizni(0); //ileri
		if(buffer[0]=='1') hareketizni(2); //sağ
		if(buffer[0]=='2') hareketizni(1); //geri
		if(buffer[0]=='3') hareketizni(3); //sol
		if(buffer[0]=='4') guc(0); // güç arttırılıyır
		if(buffer[0]=='5'){
			kapat();
			close(newsockfd);
			close(sockfd);
			exit(0);
		}
		if(buffer[0]=='6') guc(1); // güç azaltılıyor
		if(buffer[0]=='7') durdur();
		if(buffer[0]=='8') guc(2); // roll ve pitchin başlangıç değerleri belirleniyor
	}
}

void VecihiHurkus::baglantikur(){
	cout<<"baglanti kurulmaya çalışılıyor\n";
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0){
		printf("ERROR opening socket");
		close(sockfd);
		exit(1);
	}
	bzero((char *) &serv_addr, sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);
	if (bind(sockfd, (struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) {
		printf("bindin err\n");
		close(sockfd);
		exit(1);
	}
	listen(sockfd,5);
	socklen_t clilen = sizeof(cli_addr);
	newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen);
	if (newsockfd < 0){
		printf("Kabulde hata!\n");
		close(sockfd);
		exit(1);
	}
	printf("bağlantı geldi\n");
}
