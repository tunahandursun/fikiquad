#include<iostream>
#include<fstream>
#include<stdlib.h>
#include<unistd.h>
#include<stdio.h>
#include "MotionSensor.h"
#include<thread>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include<math.h>

#define delay_ms(a) usleep(a*1000)
#define motorsayisi 4
#define minsinyal 30
#define maxsinyal 100
#define harekethassasiyeti 0.01

using namespace std;

class VecihiHurkus{
	private:
		int sockfd, newsockfd, portno; // , clilen;
		char buffer[1];
		struct sockaddr_in serv_addr, cli_addr;
		int aktifhareket;
		/*
			aktifhareket = 0 => güç arttırılıyor
			aktifhareket = 1 => güç azalıyor
		*/
		float zerovalueroll = 0;
		float zerovaluepitch = 0;

	public:
		ofstream fid;
		float motorsinyalleri[motorsayisi];
		int motorid[motorsayisi];

		void set();
		void kapat();
		void baglantikur();
		void iletisim();
		void hareketizni(int);
		void guc(int);
		void dengele(); // haraketler.cpp ( quadronun sabit durmasını sağlayak fonksyon )
		void durdur(); // motorların minimum seviyeye çekilmesini sağlar. Direk durdurmak yerine yavaşlatarak durdurmayı sağla
		void dengemerkezi(); // kaptanj.cpp ( jiroskopla hareketi dengeleyen zamazingo )

		VecihiHurkus(int port){
			cout<<"dosya açılıyor...";
			fid.open("/dev/servoblaster",ios::out);
			if(!fid){
				printf("blaster dosyası açılamadı\n");
				exit(1);
			}
			cout<<"başarılı\n";

			motorid[0] = 4;
			motorid[1] = 5;
			motorid[2] = 6;
			motorid[3] = 7;

			int i;
			for (i=0;i<motorsayisi;i++)
				motorsinyalleri[i]=minsinyal;
			cout<<"Minimum sinyal atanıyor\n";
			set();
			sleep(1);

			portno = port;
			threadler();
		}
		void threadler(){
			baglantikur();

			thread thread1(&VecihiHurkus::iletisim,this);
			sleep(1);
			thread thread2(&VecihiHurkus::dengemerkezi,this);

			thread1.join();
			thread2.join();
		}
		~VecihiHurkus(){
			kapat();
		}
};
